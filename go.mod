module gitlab.com/finservice-public/gtoggl-api

go 1.12

require (
	github.com/dougEfresh/gtoggl-api v0.0.0-20190813143908-d4a77f6fd0a6
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/throttled/throttled v2.2.4+incompatible
)
